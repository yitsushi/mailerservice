package main

import (
	"./service"
	"errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

func getConfig() (service.Config, error) {
	yamlPath := "config.yaml"
	config := service.Config{}

	if _, err := os.Stat(yamlPath); err != nil {
		return config, errors.New("config path not valid")
	}

	ymlData, err := ioutil.ReadFile(yamlPath)
	if err != nil {
		return config, err
	}

	err = yaml.Unmarshal([]byte(ymlData), &config)
	return config, err
}

func main() {
	cfg, err := getConfig()
	if err != nil {
		log.Fatal(err)
		return
	}

	svc := service.MailerService{}

	if err = svc.Run(cfg); err != nil {
		log.Fatal(err)
	}
}
