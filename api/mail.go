package api

type Mail struct {
  Id          string  `json:"id"`
  Created     int32   `json:"created"`
  Status      string  `json:"status"`
  Subject     string  `json:"subject"`
  FromEmail   string  `json:"fromEmail"`
  FromName    string  `json:"fromName"`
  ToEmail     string  `json:"ToEmail"`
  ToName      string  `json:"ToName"`
  HTML        string  `json:"html"`
  Text        string  `json:"text"`
}

