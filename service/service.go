package service

import (
	"../api"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/go-sql-driver/mysql"
)

type Config struct {
	SvcHost        string
	DbUser         string
	DbPassword     string
	DbHost         string
	DbName         string
	MandrillApiKey string
}

type MailerService struct {
}

func (s *MailerService) getDb(cfg Config) (gorm.DB, error) {
	connectionString := cfg.DbUser + ":" + cfg.DbPassword + "@tcp(" + cfg.DbHost + ":3306)/" + cfg.DbName + "?charset=utf8&parseTime=True"
	return gorm.Open("mysql", connectionString)
}

func (s *MailerService) Run(cfg Config) error {
	db, err := s.getDb(cfg)

	if err != nil {
		return err
	}

	//db.SingularTable(true)

	db.AutoMigrate(&api.Mail{})

	r := gin.Default()

	mailerResource := &MailerResource{db: db, apiKey: cfg.MandrillApiKey}

	r.POST("/send", mailerResource.send)
	r.GET("/mail/list", mailerResource.list)

	r.Run(cfg.SvcHost)
	return nil
}
