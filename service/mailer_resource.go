package service

import (
  "../api"
	"github.com/gin-gonic/gin"
  m "github.com/keighl/mandrill"
  "log"
  "github.com/jinzhu/gorm"
  "time"
)

type MailerResource struct {
  db gorm.DB
  apiKey string
}

type MailRecipientJson struct {
  Email string `"json":"email" binding:"required"`
  Name string `"json":"name" binding:"required"`
}

type RawEmailJson struct {
  To MailRecipientJson `"json":"to" binding:"required"`
  From MailRecipientJson `"json":"from" binding:"required"`
  Subject string `"json":"subject" binding:"required"`
  HTML string `"json":"html"`
  Text string `"json":"text"`
}

func (mr *MailerResource) send(c *gin.Context) {
  client := m.ClientWithKey(mr.apiKey)

  var json RawEmailJson
  c.Bind(&json)

  message := &m.Message{}
  message.AddRecipient(json.To.Email, json.To.Name, "to")
  message.FromEmail = json.From.Email
  message.FromName = json.From.Name
  message.Subject = json.Subject
  message.HTML = json.HTML
  if json.Text != "" {
    message.Text = json.Text
  }

  responses, _, _ := client.MessagesSend(message)

  mailLog := api.Mail{}

  mailLog.Id = responses[0].Id
  mailLog.Created = int32(time.Now().Unix())
  mailLog.Status = responses[0].Status
  mailLog.ToEmail = json.To.Email
  mailLog.ToName = json.To.Name
  mailLog.FromEmail = json.From.Email
  mailLog.FromName = json.From.Name
  mailLog.Subject = json.Subject
  mailLog.HTML = json.HTML
  mailLog.Text = json.Text

  if mailLog.Text == "" {
    mailLog.Text = "!No text content!"
  }

  mr.db.Create(&mailLog)

  log.Printf("[MailId:%s] Email %s to %s from %s with subject: %s",
    mailLog.Id, mailLog.Status, json.To.Email, json.From.Email, json.Subject)

	c.JSON(200, mailLog)
}

func (mr *MailerResource) list(c *gin.Context) {
  var mails []api.Mail
  mr.db.Order("created desc").Limit(50).Find(&mails)
  c.JSON(200, mails)
}
